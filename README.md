# README.md

UX mockups and wireframes for [Snowdrift.coop](https://snowdrift.coop/).


## License

HTML: AGPLv3 / Media: CC-BY-SA 4.0
